"""Builds the MNIST network.
Implements the inference/loss/training pattern for model building.
1. inference() - Builds the model as far as is required for running the network
forward to make predictions.
2. loss() - Adds to the inference model the layers required to generate loss.
3. training() - Adds to the loss model the Ops required to generate and
apply gradients.
This file is used by the various "fully_connected_*.py" files and not meant to
be run.
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import tensorflow as tf
from tensorflow.contrib import layers as tflayers
from tensorflow.python.framework import dtypes
from sklearn.metrics import mean_squared_error


"""
Forms the network model where it may be used for inference.
Args:
    data: a numpy array of scalars
    num_units: number of sequential input data to use
    rnn_layers: a list of dictionaries.
        - each dictionary contains:
            + 'num_units': number of units in the cell
            + 'keep_prob': used for dropout
    dense_layers: a list of scalars representing the number of units
        in each dense layer added to the top of the LSTM.
Returns:
    output: the output tensor with computed targets
"""

def do_inference(data, num_units, rnn_layers, dense_layers=None):

    """
    Creates a stack of LSTM cells.
    Args:
        layers: a list of dictionaries.
            - each dictionary contains:
                + 'num_units': number of units in the cell
                + 'keep_prob': used for dropout
    Returns:
        stacked_cells: a list of basic LSTM cells with dropout enabled
    """
    def stack_lstm_cells(layers):

        # empty cell list
        stacked_cells = []

        # create each cell, then stack to list
        for layer in layers:
            # create a basic LSTM cell
            basic_cell = tf.contrib.rnn.BasicLSTMCell(layer['num_units'],
                                                      state_is_tuple=True)
            # configure for dropout
            cell = tf.contrib.rnn.DropoutWrapper(basic_cell,
                                                 layer['keep_prob'])
            stacked_cells.append(cell)
        return stacked_cells

    """
    Create dense layers.
    Args:
        input_layers: a tensor that serves as the input to the dense layer
        layers: a list of dictionaries for each dense layer.
            - each dictionary contains:
                'activation': the activation function to use at the layer
                'dropout': dropout probability
    """
    def dnn_layers(input_layers, layers):

        # if we want to create dense layers
        if layers:
            return tflayers.stack(input_layers,
                                  tflayers.fully_connected,
                                  layers['layers'],
                                  activation=layers.get('activation'),
                                  dropout=layers.get('dropout'))

        # if no dense layers, just return the input
        else:
            return input_layers

    # create a stacked LTSM architecture
    stacked_lstm = tf.contrib.rnn.MultiRNNCell(stack_lstm_cells(rnn_layers),
                                               state_is_tuple=True)

    # unpack data
    x_ = tf.unstack(data, axis=1, num=num_units)

    # send data through LSTM layers
    output, layers = tf.contrib.rnn(stacked_lstm, x_, dtype=dtypes.float32)

    # send data through dense layer
    output = dnn_layers(output[-1], dense_layers)

    return output


def do_loss(pred, targ):
    """Calculates the loss from the network prediction and the targets.
    Args:
        logits: Logits tensor, float - [batch_size, NUM_CLASSES].
        labels: Labels tensor, int32 - [batch_size].
    Returns:
        loss: Loss tensor of type float.
    """
    targ = tf.to_int64(targ)
    loss = tf.nn.l2_loss(pred - targ, name='l2_loss')
    return tf.reduce_mean(loss, name='l2_loss_mean')


def training(loss, learning_rate):
    """Sets up the training Ops.
    Creates a summarizer to track the loss over time in TensorBoard.
    Creates an optimizer and applies the gradients to all trainable variables.
    The Op returned by this function is what must be passed to the
    `sess.run()` call to cause the model to train.
    Args:
        loss: Loss tensor, from loss().
        learning_rate: The learning rate to use for gradient descent.
    Returns:
        train_op: The Op for training.
    """
    # Add a scalar summary for the snapshot loss.
    tf.summary.scalar('loss', loss)
    # Create the gradient descent optimizer with the given learning rate.
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    # Create a variable to track the global step.
    global_step = tf.Variable(0, name='global_step', trainable=False)
    # Use the optimizer to apply the gradients that minimize the loss
    # (and also increment the global step counter) as a single training step.
    train_op = optimizer.minimize(loss, global_step=global_step)
    return train_op


def evaluation(pred, targs):
    """
    Evaluate the quality of the network using MSE.
    Args:
        pred: prediction from network
        targs: target values
    returns:
        a scalar representing the RMSE of the predictions
    """
    rmse = mean_squared_error(targs, pred)
    # Return the number of true entries.
    return tf.reduce_sum(tf.cast(rmse, tf.int32))
