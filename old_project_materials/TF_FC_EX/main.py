import argparse
import sys
import tensorflow as tf
import run_fcns as rf
import global_settings as gs


def main(_):
    if tf.gfile.Exists(gs.FLAGS.log_dir):
        tf.gfile.DeleteRecursively(gs.FLAGS.log_dir)
    tf.gfile.MakeDirs(gs.FLAGS.log_dir)
    rf.run_training()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '--learning_rate',
            type=float,
            default=0.01,
            help='Initial learning rate.'
            )
    parser.add_argument(
            '--max_steps',
            type=int,
            default=20000,
            help='Number of steps to run trainer.'
            )
    parser.add_argument(
            '--hidden1',
            type=int,
            default=128,
            help='Number of units in hidden layer 1.'
            )
    parser.add_argument(
            '--hidden2',
            type=int,
            default=32,
            help='Number of units in hidden layer 2.'
            )
    parser.add_argument(
            '--batch_size',
            type=int,
            default=100,
            help='Batch size.  Must divide evenly into the dataset sizes.'
            )
    parser.add_argument(
            '--input_data_dir',
            type=str,
            default='/tmp/tensorflow/mnist/input_data',
            help='Directory to put the input data.'
            )
    parser.add_argument(
            '--log_dir',
            type=str,
            default='/tmp/tensorflow/mnist/logs/fully_connected_feed',
            help='Directory to put the log data.'
            )
    parser.add_argument(
            '--fake_data',
            default=False,
            help='If true, uses fake data for unit testing.',
            action='store_true'
            )

    gs.FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
