'''
###############################################################################
i.) Write a function that performs convolution on an image slice.
    - Inputs: image array, filter array
    - Outputs: image array

* assume that the filter has an odd number of pixels in the m and n directions
###############################################################################
ii.) Write a main function that will read in an image, and:
- Allow the user to specify the filter size, and coefficients. 
- Next, the convolution function will be called to filter the image.
- Next, the original image will be shown alongside the filtered image.
- Next, the filtered image will be saved as a .jpg.
- Next, the Fourier transform of the original and filtered images will be
calculated, and the magnitude will be plotted.
- Next, the frequency response of the filter will be plotted.

'''

import numpy as np
import cv2
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


###############################################################################
def conv2(img, filt):

    # find size of image
    M, N = img.shape

    # find size of filter
    K, L = filt.shape
    K_half = int(np.floor(K/2.))
    L_half = int(np.floor(L/2.))

    # reverse the filter
    rev_filt = np.flipud(np.fliplr(filt))

    # pad image with 0's along the boundaries
    padded_img = np.zeros([M+2*K_half, N+2*L_half])
    padded_img[K_half:-K_half, L_half:-L_half] += img

    # perform convolution
    filt_img = np.empty([M, N])
    for i in range(M):
        for j in range(N):
            filt_img[i, j] = np.sum(rev_filt.astype(float) *
                                    padded_img[i:i+K, j:j+L])

    return filt_img

###############################################################################

# load file
file_name = 'lena.bmp'
img = cv2.imread(file_name, 0)


# user defined filter
filt = []
K, L = input('filter dimensions? (comma separated):    ')
temp = range(L)
for i in range(K):
    temp = input('enter filter row number {}:    '.format(i+1))
    filt.append(list(temp))
filt = np.array(filt)

'''
filter_one = np.array([[1, 2, 1],[2, 4, 2],[1, 2, 1]])/16.
filter_two = np.array([[-1, -1, -1],[-1, 8, -1],[-1, -1, -1]])
filter_three = np.array([[0, -1, 0],[-1, 5, -1],[0, -1, 0]])
'''
# for saving files
filt_num = 3

# calculate the Fourier transform of the original
FT_img = np.fft.fft2(img)
FT_img = np.fft.fftshift(FT_img)
FT_img_mag = 10*np.log(np.abs(FT_img))

# perform the filtering
filt_img = conv2(img, filt)

# adjust range and convert to unsigned 8 bit
max_val = np.max(filt_img)
min_val = np.min(filt_img)
filt_img = np.round((filt_img-min_val)*255/(max_val-min_val), 0)
filt_img = filt_img.astype(np.uint8)

# calculate the Fourier transform of the filtered
FT_img_filt = np.fft.fft2(filt_img)
FT_img_filt = np.fft.fftshift(FT_img_filt)
FT_img_mag_filt = 10*np.log(np.abs(FT_img_filt))

# calculate the frequency response of the filter
filt_freq_rsp = np.fft.fft2(filt, img.shape)
filt_freq_rsp = np.fft.fftshift(filt_freq_rsp)
filt_freq_rsp_mag = np.abs(filt_freq_rsp)

# plot
plt.figure(1)
plt.clf()
plt.subplot(2, 2, 1)
plt.title('Original')
plt.imshow(img, cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 2)
plt.title('|FT| of Original')
plt.imshow(FT_img_mag, cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 3)
plt.title('Filtered (Filter #{}'.format(filt_num))
plt.imshow(filt_img, cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 4)
plt.title('|FT| of Filtered (Filter #{})'.format(filt_num))
plt.imshow(FT_img_mag_filt, cmap='gray')
plt.axis('off')
plt.savefig('filter_{}.pdf'.format(filt_num), dpi=800)

# save jpg
cv2.imwrite('lena_f{}.jpg'.format(filt_num), filt_img)

# plot 3d frequency response
plt.figure(2)
plt.clf()
fig = plt.figure()
ax = fig.gca(projection='3d')
x = np.linspace(-1, 1, img.shape[0])
x, y = np.meshgrid(x, x)
surf = ax.plot_surface(x, y, filt_freq_rsp_mag, cmap=cm.coolwarm,
                       linewidth=0, antialiased=False)

ax.set_ylabel('Frequency')
ax.set_xlabel('Frequency')
ax.set_zlabel('Magnitude')
plt.title('Frequenct Response, Filter #{}'.format(filt_num))
plt.tight_layout()
plt.savefig('filter_{}_freq_rsp.png'.format(filt_num), dpi=800)

# plot 2d frequency response
plt.figure(3)
plt.clf()
plt.imshow(filt_freq_rsp_mag, cmap=cm.coolwarm)
plt.axis('off')
plt.title('Frequenct Response, Filter #{}'.format(filt_num))
plt.savefig('filter_{}_freq_rsp_2d.png'.format(filt_num))
