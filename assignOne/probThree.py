'''
###############################################################################
i.) Use the convolution function from Problem One, using a Gaussian filter
of size n x n.
    n = 3, 5, 7, 9
    sigma = 0.1, 0.01
'''

import numpy as np
import cv2
from matplotlib import pyplot as plt
from matplotlib import cm
from scipy import signal


###############################################################################
def conv2(img, filt):

    # find size of image
    M, N = img.shape

    # find size of filter
    K, L = filt.shape
    K_half = int(np.floor(K/2.))
    L_half = int(np.floor(L/2.))

    # reverse the filter
    rev_filt = np.flipud(np.fliplr(filt))

    # pad image with 0's along the boundaries
    padded_img = np.zeros([M+2*K_half, N+2*L_half])
    padded_img[K_half:-K_half, L_half:-L_half] += img

    # perform convolution
    filt_img = np.empty([M, N])
    for i in range(M):
        for j in range(N):
            filt_img[i, j] = np.sum(rev_filt.astype(float) *
                                    padded_img[i:i+K, j:j+L])

    return filt_img

###############################################################################

# load file
file_name = 'lena.bmp'
img = cv2.imread(file_name, 0)

sigma_list = [.1, .01]
n_list = [3, 5, 7, 9]

for sigma in sigma_list:
    for n in n_list:

        # create noisy image
        noisey_img = img + 255*np.random.normal(0, sigma, img.shape)

        # define filter
        filt_size = [n, n]

        # gaussian filter
        s = 0.5
        x = np.linspace(-(n/2), n/2, n).reshape((1, n))
        gauss = np.exp(-(x*x)/(2*s*s))
        gauss2 = gauss.T * gauss
        filt = gauss2/np.sum(gauss2)

        # perform the filtering
        filt_img = conv2(noisey_img, filt)

        # adjust range and convert to unsigned 8 bit
        max_val = np.max(filt_img)
        min_val = np.min(filt_img)
        filt_img = np.round((filt_img-min_val)*255/(max_val-min_val), 0)
        filt_img = filt_img.astype(np.uint8)

        # calculate the frequency response of the filter
        filt_freq_rsp = np.fft.fft2(filt, img.shape)
        filt_freq_rsp = np.fft.fftshift(filt_freq_rsp)
        filt_freq_rsp_mag = np.abs(filt_freq_rsp)

        # plot
        # noisy
        plt.figure(1)
        plt.clf()

        # frequency response of filter
        plt.subplot(1, 3, 2)
        plt.title('Gauss. Filter Freq. Resp. (s={})'.format(s), fontsize=10)
        plt.imshow(filt_freq_rsp_mag, cmap=cm.cool)
        plt.axis('off')

        plt.subplot(1, 3, 1)
        plt.title('Noisey (sigma={})'.format(sigma), fontsize=10)
        plt.imshow(noisey_img, cmap='gray')
        plt.axis('off')

        # filtered
        plt.subplot(1, 3, 3)
        plt.title('Filtered (n={})'.format(n), fontsize=10)
        plt.imshow(filt_img, cmap='gray')
        plt.axis('off')

        # save plot
        plt.tight_layout()
        plt.savefig('gauss_sigma={}_n={}_s={}.pdf'.format(sigma, n, s),
                    dpi=1000)
