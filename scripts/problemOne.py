import cv2
import numpy as np
from matplotlib import pyplot as plt

og_file = 'us.jpg'
eq_file = og_file[:-4] + '_eq.jpg'

# read in image
img = cv2.imread(og_file)

# form histograms
color = ('b', 'g', 'r')         # OpenCV represents RGB images in reverse order... BGR
hist = np.empty([256, 3])
for i, col in enumerate(color):
    hist[:, i] = np.array(cv2.calcHist([img], [i], None, [256], [0, 256]).tolist())[:, 0] # no mask    
    plt.plot(hist[:, i], color = col)
    plt.xlim([0, 256])
plt.show()

# form CDF
cdf = hist.cumsum(axis=0)

# normalize CDF
cdf = 255 * cdf / cdf[-1, :]

# use linear interpolation of CDF to find new pixel values
img_eq = np.empty(img.shape)
for i, col in enumerate(color):
    img_eq[:, :, i] = np.interp(img[:, :, i].flatten(), range(256), cdf[:, i]).reshape(img[:, :, i].shape)

# revert from BGR to RGB, and display the original image
plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))

# convert from float64 to uint8
equalized = cv2.convertScaleAbs(img_eq)

# revert from BGR to RGB, and display the equalized image
plt.imshow(cv2.cvtColor(equalized, cv2.COLOR_BGR2RGB))

# save the equalized image
cv2.imwrite(eq_file, equalized)

# plot the equalized histograms
hist_eq = np.empty([256, 3])
for i, col in enumerate(color):
    hist_eq[:, i] = np.array(cv2.calcHist([equalized], [i], None, [256], [0, 256]).tolist())[:, 0] # no mask    
    plt.plot(hist_eq[:, i], color = col)
    plt.xlim([0, 256])
plt.show()



