import h5py
from matplotlib import pyplot as plt
import numpy as np
import cPickle as pkl
import tensorflow as tf

FLAGS = tf.app.flags.FLAGS

class brain_data:
    
    def __init__(self):
        stats_file = 'ECOG_4041_mean.pkl'
        file_name = 'ECOG_40_41.h5'
        stats = pkl.load(open(stats_file, 'r'))
        f = h5py.File(file_name, 'r')
        # f.items() will show the datasets
        train_data = f['train']
        train_targ = f['train_label']
        # original image is 20 x 18... reshape
        data = np.float32(train_data[:]*stats['scale_factor'])
        data = 2*(data - stats['min'])/(stats['max'] - stats['min']) - 0.5
        f.close()
        # get rid of first 5000 samples
        self.data = data[5000:, :]
        self.get_batch
    
    def get_batch(self):
        seq_len = FLAGS.seq_length
        batch_sz = FLAGS.batch_size
        seeds = np.random.randint(0, self.data.shape[0]-seq_len, (batch_sz, 1))
        batch = np.empty((batch_sz, seq_len, 20, 18, 1))
        for i in range(batch_sz):
            batch[i, :, :, :, 0] = self.data[seeds[i]:seeds[i]+seq_len, :].reshape(seq_len, 20, 18)
        return batch
