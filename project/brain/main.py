from generator_class import *
import numpy as np
#import data_handler as dh
import load as dh
import tensorflow as tf
import numpy as np
import skvideo.io
from matplotlib import pyplot as plt

FLAGS = tf.app.flags.FLAGS

tf.app.flags.DEFINE_string('train_dir', './trained_model',
                           """ directory to store trained model""")
tf.app.flags.DEFINE_string('mode', 'test',
                           """ train or test """)
tf.app.flags.DEFINE_integer('gen_len', 50,
                            """number of frames to generate in test mode""")
tf.app.flags.DEFINE_integer('seq_length', 10,
                            """size of hidden layer""")
tf.app.flags.DEFINE_integer('seq_start', 5,
                            """ start of seq generation""")
tf.app.flags.DEFINE_integer('max_step', 20001,
                            """max num of steps""")
tf.app.flags.DEFINE_float('keep_prob', .8,
                          """for dropout""")
tf.app.flags.DEFINE_float('learning_rate', 1e-5,
                          """for dropout""")
tf.app.flags.DEFINE_integer('batch_size', 16,
                            """batch size for training""")
tf.app.flags.DEFINE_float('weight_init', .1,
                          """weight init for fully connected layers""")

# configure input parameters... stored as flags
if tf.gfile.Exists(FLAGS.train_dir):
    tf.gfile.DeleteRecursively(FLAGS.train_dir)
tf.gfile.MakeDirs(FLAGS.train_dir)

# make input tensor, and wrap it with dropout
x = tf.placeholder(tf.float32, [None, FLAGS.seq_length, 20, 18, 1])
keep_prob = tf.placeholder("float")
x_dropout = tf.nn.dropout(x, keep_prob)

model = generator(x_dropout)

# define an initialization operation
init_op = tf.global_variables_initializer()

# create saving operation
saver = tf.train.Saver()

# run operations on the graph
with tf.Session() as sess:

    print('loading data')
    brain_images = dh.brain_data()
    
    print('building network')
    sess.run(init_op)

    if FLAGS.mode == 'train':
        loss = np.empty(FLAGS.max_step)
        # run specified number of training steps
        for step in xrange(FLAGS.max_step):

            # generate a batch of training data
            data = brain_images.get_batch()
            

            # run training and loss operations on the graph
            _, loss_r = sess.run([model.optimize, model.loss],
                                 feed_dict={x: data,
                                            keep_prob: FLAGS.keep_prob})
            loss[step] = loss_r

            # print loss for troubleshooting... should be decreasing in trend
            if step % 10 == 0:
                print('======= step number {} ======='.format(step))
                print('loss: {}'.format(loss_r))
            if step % 1000 == 0:
                save_path = saver.save(sess, FLAGS.train_dir)
                print('model saved in file {}'.format(save_path))

            assert not np.isnan(loss_r), 'Model diverged with loss = NaN'
        save_path = saver.save(sess, FLAGS.train_dir)
        print('model saved in file {}'.format(save_path))

        plt.figure(figsize=(8, 8))
        plt.plot(loss)
        plt.title('L2 Loss', fontsize=24)
        plt.xlabel('Iterations', fontsize=18)
        plt.ylabel('Loss', fontsize=18)
        plt.savefig('l2_loss_iter={}.png'.format(FLAGS.max_step))

    if FLAGS.mode == 'test':
        saver.restore(sess, FLAGS.train_dir)
        print('model restored')

        # generate a random seed
        seed = np.random.randint(0, brain_images.data.shape[0] -
                                 FLAGS.gen_len, 1)

        # get example sequence
        true_data = brain_images.data[seed:seed+FLAGS.gen_len,
                                      :].reshape(FLAGS.gen_len, 20, 18)

        # start sequence
        data = np.zeros((FLAGS.batch_size, FLAGS.seq_length, 20, 18, 1))
        data[0, :, :, :, 0] = true_data[:FLAGS.seq_length, :, :]

        # some video stuff
        video = np.zeros([FLAGS.gen_len, 20, 18, 1])

        # loop over generate length
        for i in range(FLAGS.gen_len):

            # predict the next 'seq_length - seq_start' frames
            pred = sess.run([model.prediction], feed_dict={x: data,
                            keep_prob: 1.0})

            # we only want to use the prediction forecasted at time t+1
            pred = pred[0][0] # only use first sequence in batch
            pred_one = pred[FLAGS.seq_start, :, :, :]

            # update data
            data = np.roll(data, -1, axis=1)
            data[0, FLAGS.seq_start-1, :, :, :] = pred_one

            # write video frame
            frame = np.uint8(data[0, 0, :, :, :] * 255)
            video[i, :, :, :] = frame
            
        skvideo.io.vwrite('gen_video.mp4', video, backend='libav')
