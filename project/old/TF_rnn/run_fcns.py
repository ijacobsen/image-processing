"""Trains and Evaluates the MNIST network using a feed dictionary."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# pylint: disable=missing-docstring
import os.path
import time
import global_settings as gs

from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import arch_des as ad
import numpy as np


def placeholder_inputs():
    
    data_placeholder = tf.placeholder(tf.float32, shape=(gs.FLAGS.batch_size,
                                                         gs.FLAGS.time_steps))
    targs_placeholder = tf.placeholder(tf.float32, shape=(gs.FLAGS.batch_size))

    return data_placeholder, targs_placeholder


def fill_feed_dict(data_pl, targs_pl):

    data_feed = np.vstack((np.linspace(0, 1, 5), np.linspace(10, 11, 5)))
    targs_feed = np.vstack((1.0, 2.0))
    
    feed_dict = {
            data_pl: data_feed,
            targs_pl: targs_feed,
                }
    return feed_dict


def run_training():

    # Tell TensorFlow that the model will be built into the default Graph.
    with tf.Graph().as_default():
        # Generate placeholders for the images and labels.
        data_placeholder, targs_placeholder = placeholder_inputs()

        # Build a Graph that computes predictions from the inference model.
        preds = ad.do_inference(data_placeholder,
                             gs.FLAGS.time_steps,
                             gs.FLAGS.rnn_layers)

        # Add to the Graph the Ops for loss calculation.
        loss = ad.do_loss(preds, targs_placeholder)

        # Add to the Graph the Ops that calculate and apply gradients.
        train_op = ad.do_training(loss, gs.FLAGS.learning_rate)

        # Add the Op to compare the logits to the labels during evaluation.
        eval_correct = ad.do_evaluation(preds, targs_placeholder)

        # Add the variable initializer Op.
        init = tf.global_variables_initializer()

        # Create a session for running Ops on the Graph.
        sess = tf.Session()

        # And then after everything is built:

        # Run the Op to initialize the variables.
        sess.run(init)

        # Start the training loop.
        for step in xrange(gs.FLAGS.max_steps):
            start_time = time.time()

            # Fill a feed dictionary with the actual set of images and labels
            # for this particular training step.
            feed_dict = fill_feed_dict(data_placeholder,
                                       targs_placeholder)

            # Run one step of the model.  The return values are the activations
            # from the `train_op` (which is discarded) and the `loss` Op.  To
            # inspect the values of your Ops or variables, you may include them
            # in the list passed to sess.run() and the value tensors will be
            # returned in the tuple from the call.
            _, loss_value = sess.run([train_op, loss],
                                     feed_dict=feed_dict)

            duration = time.time() - start_time

            # Write the summaries and print an overview fairly often.
            if step % 100 == 0:
                # Print status to stdout.
                print('Step %d: loss = %.2f (%.3f sec)' % (step,
                                                           loss_value,
                                                           duration))