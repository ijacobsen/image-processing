import argparse
import sys
import tensorflow as tf
import run_fcns as rf
import global_settings as gs


def main(_):
    if tf.gfile.Exists(gs.FLAGS.log_dir):
        tf.gfile.DeleteRecursively(gs.FLAGS.log_dir)
    tf.gfile.MakeDirs(gs.FLAGS.log_dir)
    rf.run_training()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
            '--learning_rate',
            type=float,
            default=0.01,
            help='Initial learning rate.'
            )
    parser.add_argument(
            '--time_steps',
            type=int,
            default=5,
            help='Size of cells.'
            )
    parser.add_argument(
            '--max_steps',
            type=int,
            default=1000,
            help='Number of steps to run trainer.'
            )
    parser.add_argument(
            '--rnn_layers',
            type=int,
            default=[{'num_units': 5,
                     'keep_prob': 1}],
            help='List of dictionaries for stacked RNN layers.'
            )
    parser.add_argument(
            '--dense_layers',
            type=int,
            default=None,
            help='List of ints for dense layers.'
            )
    parser.add_argument(
            '--batch_size',
            type=int,
            default=2,
            help='Batch size.  Must divide evenly into the dataset sizes.'
            )
    parser.add_argument(
            '--log_dir',
            type=str,
            default='/tmp/tensorflow/mnist/logs/fully_connected_feed',
            help='Directory to put the log data.'
            )

    gs.FLAGS, unparsed = parser.parse_known_args()
    tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
