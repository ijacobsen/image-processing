import numpy as np
import db_wt as dbwt
from matplotlib import pyplot as plt

# load data
filename = 'ecg.txt'
x = np.loadtxt(filename)

# how many levels
m = 5

# which dbt to use
k = 5

# forward transform
[c, d] = dbwt.forward(x, m, k)

# inverse transform
x_ = dbwt.inverse(c, d, k)

# plot error
plt.figure(figsize=(8, 8))
plt.plot(x - x_)
plt.xlabel('Time (samples)')
plt.ylabel('Error')
plt.title('Reconstruction Error of {}-level db-{} Transform'.format(m, k),
          fontsize=18)
plt.tight_layout()
