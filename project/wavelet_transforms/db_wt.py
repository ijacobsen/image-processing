import daubechies_filters as dbf
import filter_banks as fb


def forward(x, m, k):

    # find analysis and synthesis coefficients... we'll only use analysis
    [h0, h1, g0, g1] = dbf.db_filters(k)

    # c = x at the first level
    c = x
    d = []

    # analysis (forward transform)
    for i in range(m):
        [c, d_n] = fb.analysis_fb(h0, h1, c)
        d.append(d_n)

    return [c, d]


def inverse(c, d, k):

    m = len(d)

    # find analysis and synthesis coefficients... we'll only use analysis
    [h0, h1, g0, g1] = dbf.db_filters(k)

    # synthesis (inverse transform)
    for i in range(m-1, -1, -1):
        c = fb.synthesis_fb(g0, g1, c, d[i])

    return c
