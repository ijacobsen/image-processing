import h5py
from matplotlib import pyplot as plt
import numpy as np
import cPickle as pkl

stats_file = 'ECOG_4041_mean.pkl'
file_name = 'ECOG_40_41.h5'

stats = pkl.load(open(stats_file, 'r'))

f = h5py.File(file_name, 'r')
# f.items() will show the datasets
train_data = f['train']
train_targ = f['train_label']

# original image is 20 x 18... reshape
data = np.float32(train_data[:]*stats['scale_factor'])
data = 2*(data - stats['min'])/(stats['max'] - stats['min']) - 0.5

plt.imshow(data[-2, :].reshape((20, 18)))

f.close()
