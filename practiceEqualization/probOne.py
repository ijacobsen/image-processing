#   i.) read a color image into a matrix
#  ii.) convert to 8-bit grayscale image using RGB to gray conversion formula
# iii.) generate a digital negative version of the image
#  iv.) display both the color image, the gray scale, and the negative

import numpy as np
import cv2
from matplotlib import pyplot as plt

# variables
file_name = 'us.jpg'

# i.) read color image into a matrix
img = cv2.imread(file_name)

# ii.) convert color to grayscale
# the correct way to do this is to use the luminosity method, i.e. 
# img = (R * 0.3) + (G * 0.59) + (B * 0.11)
g_img = cv2.convertScaleAbs(0.11*img[:, :, 0] + 0.59*img[:, :, 1] + 0.3*img[:, :, 2])

# iii.) generate a digital negative
i_img = 255 - img

# iv.) plot all three
plt.subplot(3, 1, 1)
plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
plt.subplot(3, 1, 2)
plt.imshow(cv2.cvtColor(g_img, cv2.COLOR_GRAY2BGR))
plt.subplot(3, 1, 3)
plt.imshow(cv2.cvtColor(i_img, cv2.COLOR_BGR2RGB))
plt.show()

# v.) save gray image
cv2.imwrite(file_name[:-4]+'_gray.jpg', g_img)
