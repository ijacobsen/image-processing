import numpy as np
import cv2
from matplotlib import pyplot as plt

def compute_grayscale_hist(img):
    intensity_list = np.zeros([256, 1])
    for i in range(intensity_list.shape[0]):
        intensity_list[i] = np.sum(img == i)
    return intensity_list

# variables
file_name = 'us.jpg'

# i.) read color image into a matrix
img = cv2.imread(file_name)

# ii.) convert color to grayscale
# the correct way to do this is to use the luminosity method, i.e. 
# img = (R * 0.3) + (G * 0.59) + (B * 0.11)
g_img = cv2.convertScaleAbs(0.11*img[:, :, 0] + 0.59*img[:, :, 1] + 0.3*img[:, :, 2])


# iii.) compute histogram
hist = compute_grayscale_hist(g_img)

# iv.) gray histogram equalization

# form CDF
cdf = hist.cumsum(axis=0)

# normalize CDF
cdf = 255 * cdf / cdf[-1]

# use linear interpolation of CDF to find new pixel values
img_eq = cv2.convertScaleAbs(np.interp(g_img.flatten(), range(256), cdf[:, 0]).reshape(g_img.shape))

# calculate new histogram
hist_eq = compute_grayscale_hist(img_eq)

# create subplots
plt.subplot(2, 2, 1)
plt.imshow(g_img, cmap='gray')
plt.subplot(2, 2, 2)
plt.stem(hist)
plt.subplot(2, 2, 3)
plt.imshow(img_eq, cmap='gray')
plt.subplot(2, 2, 4)
plt.stem(hist_eq)
plt.show()

# save
cv2.imwrite(file_name[:-4]+'_g.jpg', g_img)
cv2.imwrite(file_name[:-4]+'_g_eq.jpg', img_eq)
#plt.savefig('final.jpg')

