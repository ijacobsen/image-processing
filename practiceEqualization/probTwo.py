#  i.) write a function to compute the histogram of a grayscale image
# ii.) plot a grey image next to its histogram

import numpy as np
import cv2
from matplotlib import pyplot as plt

def compute_grayscale_hist(img):
    intensity_list = np.zeros([256, 1])
    for i in range(intensity_list.shape[0]):
        intensity_list[i] = np.sum(img == i)
    return intensity_list

file_name = 'us_gray.jpg'

img = cv2.imread(file_name, cv2.CV_LOAD_IMAGE_GRAYSCALE)


hist = compute_grayscale_hist(img)

plt.subplot(2, 1, 1)
plt.imshow(img, cmap='gray')   
plt.subplot(2, 1, 2)
plt.plot(hist)

