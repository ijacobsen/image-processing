'''
a.) write a program for Harris corner point detection at a fixed scale
    i.) generate gradient images of Ix and Iy using filters corresponding to
derivative of Gaussian functions of a chosen scale sigma and window size w
(let us use w = 4*sigma + 1)... use conv2() fcn
    ii.) compute three images Ix*Ix, Iy*Iy, Ix*Iy
    iii.) smooth each image using a Gaussian filter with scale 2sigma with
window size 6*sigma+1
    iv.) generate an image of Harris cornerness value by forming the moment
matrix A at every pixel based on the images from (c)
    v.) detect local maxima in the above image (for simplicity you could just
check whether a center pixel is larger than its 8 neighbors). Pick the first N
feature points with largest Harris values.
    vi.) mark each detected point using a small circle... use cv2.circle() fcn
to draw the circle... display the iamge with the detected points.
    vii.) write your own functions to generate Gaussian and derivative of
Gaussian filters from the analytical forms

b.) apply the Harris detector to a test image (use gray scale), using sigma=1,
N=50. Do the features detected make sense?

c.) starting with the previous image, generate another two images with
different rotations and another two images with different scalings (half and
double)... use cv2.resize(), cv2.getRotationMatrix2D(), cv2.warmAffine()

d.) apply Harris detector to these images. Do you detect almost the same set of
feature points in each case? why?

'''
import numpy as np
import cv2
from matplotlib import pyplot as plt
import scipy.ndimage as sc
import feature_detection_lib as fdl

# load image
file_name = 'blocks.jpg'
img = cv2.imread(file_name, 0)


# some parameters
sigma = 1
w = 4*sigma + 1

# form the gradient images
I_2x = (fdl.I_x(img, fdl.G_x(sigma, w)))**2
I_2y = (fdl.I_y(img, fdl.G_y(sigma, w)))**2
I_xy = fdl.I_x(img, fdl.G_x(sigma, w)) * fdl.I_y(img, fdl.G_y(sigma, w))

# smooth gradient images
wI_2x = fdl.wI(I_2x, sigma)
wI_2y = fdl.wI(I_2y, sigma)
wI_xy = fdl.wI(I_xy, sigma)
grad_imgs = np.vstack((np.hstack((wI_2x, wI_xy)), np.hstack((wI_xy, wI_2y))))

# plot gradient images
plt.figure(figsize=(6, 6))
plt.imshow(grad_imgs, cmap='gray')
plt.title('[[wI2x, wIxy], [wIxy, wI2y]]', fontsize=18)
plt.axis('off')
plt.savefig('grad_imgs_{}.png'.format(file_name[:-4]), dpi=100)

# form H matrix
alpha = 0.06
H = wI_2x * wI_2y - wI_xy**2 - alpha*(wI_2x + wI_2y)**2
plt.figure(figsize=(6, 6))
plt.imshow(H, cmap='gray')

# plot harris cornerness
plt.title('Harris Cornerness', fontsize=18)
plt.axis('off')
plt.savefig('harris_{}.png'.format(file_name[:-4]), dpi=100)

# thresholding
H[H < 1000] = 0
plt.imshow(H, cmap='gray')

# local maxima filtering
lm_H = sc.maximum_filter(H, (5, 5), mode='mirror')
lm_H[lm_H != H] = 0

# number of features to detect
N = 50

# sort pixel intensities, preserving index
flat_feat_locs = np.argsort(lm_H.reshape((lm_H.shape[0] *
                                          lm_H.shape[1], 1)), axis=0)[-N:]
y_feat_locs = flat_feat_locs/lm_H.shape[1]
x_feat_locs = flat_feat_locs - y_feat_locs*lm_H.shape[1]
feat_locs = map(tuple, np.hstack((y_feat_locs, x_feat_locs)))

# draw circles
h_img = np.copy(img)
for i in range(N):
    cv2.circle(h_img, (feat_locs[i][1], feat_locs[i][0]), 5, 0, 2)

# plot features
plt.figure(figsize=(6, 6))
plt.imshow(h_img, cmap='gray')
plt.title('Detected Features', fontsize=18)
plt.axis('off')
plt.savefig('features_{}.png'.format(file_name[:-4]), dpi=100)
