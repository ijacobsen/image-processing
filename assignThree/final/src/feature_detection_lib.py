import numpy as np
from scipy import signal
import scipy.ndimage as sc


def G(sigma, w):
    x = np.linspace(-(w/2), w/2, w).reshape(1, w)
    y = x.T
    X, Y = np.meshgrid(x, y)
    kern = np.exp(-(X**2 + Y**2)/(2*sigma**2))
    return kern/(2*np.pi*sigma**2)


def G_x(sigma, w):
    return -(np.linspace(-(w/2), w/2, w).reshape(1, w)/sigma**2)*G(sigma, w)


def G_y(sigma, w):
    return -(np.linspace(-(w/2), w/2, w).reshape(w, 1)/sigma**2)*G(sigma, w)


def I_x(img, filt):
    return signal.convolve2d(img, filt, mode='same', boundary='symm')


def I_y(img, filt):
    return signal.convolve2d(img, filt, mode='same', boundary='symm')


def wI(img, sigma):
    return signal.convolve2d(img, G(2*sigma, 20*sigma + 1), mode='same',
                             boundary='symm')
def L(img, sigma, w):
    return signal.convolve2d(img, G(sigma, w), mode='same', boundary='symm')

def harris_detector(img, sigma, N):

    # window size for Gaussian filter
    w = 4*sigma + 1
    
    # form the gradient images
    I_2x = (I_x(img, G_x(sigma, w)))**2
    I_2y = (I_y(img, G_y(sigma, w)))**2
    I_xy = I_x(img, G_x(sigma, w)) * I_y(img, G_y(sigma, w))

    # smooth gradient images
    wI_2x = wI(I_2x, sigma)
    wI_2y = wI(I_2y, sigma)
    wI_xy = wI(I_xy, sigma)

    # form H matrix
    alpha = 0.06
    H = wI_2x * wI_2y - wI_xy**2 - alpha*(wI_2x + wI_2y)**2

    # thresholding
    H[H < 1000] = 0

    # local maxima filtering
    lm_H = sc.maximum_filter(H, (5, 5), mode='mirror')
    lm_H[lm_H != H] = 0

    # sort pixel intensities, preserving index
    flat_feat_locs = np.argsort(lm_H.reshape((lm_H.shape[0] *
                                              lm_H.shape[1], 1)), axis=0)[-N:]
    y_feat_locs = flat_feat_locs/lm_H.shape[1]
    x_feat_locs = flat_feat_locs - y_feat_locs*lm_H.shape[1]
    feat_locs = map(tuple, np.hstack((x_feat_locs, y_feat_locs)))

    return feat_locs


def SIFT_descriptor(feats, img, sigma):

    # number of features
    N = len(feats)

    # feature vector
    feat_vec = np.zeros((N, 128))

    # for each detected feature
    for feat_num, i in enumerate(feats):

        # generate a patch of size 16x16 centered at the detected feature point
        patch = img[i[1]-8:i[1]+8, i[0]-8:i[0]+8]

        # if the feature is near an edge, skip it
        if (patch.shape != (16, 16)):
            continue

        # derive image gradient Ix and Iy
        Ix = I_x(patch, G_x(sigma, 4*sigma+1))
        Iy = I_y(patch, G_y(sigma, 4*sigma+1))

        # determine the magnitude and orientation for every pixel
        m = np.sqrt(Ix**2 + Iy**2)
        theta = np.arctan((Iy)/(Ix+0.00001))

        # multiply the gradient magnitude with a Gaussian window with sigma = 4
        weighted_mag = m * G(4, 16)

        # generate weighted orientation histogram
        count, bins = np.histogram(theta, bins=8, weights=weighted_mag)

        # dominant orientation
        d_o = np.argmax(count)

        # empty hog list... will be (16 x 8) when finished
        hog_list = []

        # each 4x4 block
        for j in range(4):
            l_j, h_j = 4*j, 4*(j+1)
            for k in range(4):
                l_k, h_k = 4*k, 4*(k+1)

                # small (4x4) magnitude
                s_m = weighted_mag[l_j:h_j, l_k:h_k]

                # small (4x4) orientation
                s_t = theta[l_j:h_j, l_k:h_k]

                # generate weighted histogram of gaussian for small block
                hog, bins = np.histogram(s_t, bins=8, weights=s_m)

                # shift hog into dominant position
                hog = np.roll(hog, -d_o, axis=0)

                # store hog
                hog_list.append(hog)

        # convert to 128 dimensional feature vector
        hog = np.array(hog_list).reshape(1, 128)

        # normalize with L2 norm
        hog = hog / np.linalg.norm(hog, ord=2)

        # nonlinear clipping
        hog[hog > 0.2] = 0.2

        # renormalize
        hog = hog / np.linalg.norm(hog, ord=2)

        # store feature descriptor
        feat_vec[feat_num, :] = hog

    return feat_vec
