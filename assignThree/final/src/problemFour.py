'''
stich two images into a panorama using SIFT feature detector and descriptor.

a.) read in an image pair
b.) detect SIFT points and extract SIFT features from each image
c.) mark the detected points in each image by a circle with radius equal to
the scale of the feature.
d.) find the corresponding point pairs between left and right images based on
their SIFT descriptors.
e.) apply RANSAC method to these matching pairs to find the largest subset of
matching pairs that are related by the same homography.
f.) create an image that shows the matching results by drawing lines between
corresponding points.
g.) apply the homography determined in (e) to the right image.
h.) stitch the transformed image from (g) and the right image together to
generate the panorama.
'''

import numpy as np
import cv2
from matplotlib import pyplot as plt

# load images
file_1 = 'DT_right.jpg'
file_2 = 'DT_left.jpg'
r_img = cv2.imread(file_1, 1)
l_img = cv2.imread(file_2, 1)

# SIFT detector and descriptor objects
detector = cv2.FeatureDetector_create("SIFT")
descriptor = cv2.DescriptorExtractor_create("SIFT")

# detect features
r_feat_loc = detector.detect(r_img)
l_feat_loc = detector.detect(l_img)

# describe features
r_feat_loc, r_feat_descr = descriptor.compute(r_img, r_feat_loc)
l_feat_loc, l_feat_descr = descriptor.compute(l_img, l_feat_loc)

# mark detected points with a circle with radius equal to the scale
r_img_mkd = np.copy(r_img)
for i in r_feat_loc:
    cv2.circle(r_img_mkd, (int(round(i.pt[0])), (int(round(i.pt[1])))),
               int(round(i.size)), (0, 0, 255), 1)
l_img_mkd = np.copy(l_img)
for i in l_feat_loc:
    cv2.circle(l_img_mkd, (int(round(i.pt[0])), (int(round(i.pt[1])))),
               int(round(i.size)), (0, 0, 255), 1)


plt.figure(figsize=(16, 8))
cat_mkd = np.hstack((l_img_mkd, r_img_mkd))
plt.imshow(cv2.cvtColor(cat_mkd, cv2.COLOR_BGR2RGB))
plt.axis('off')
plt.title('Detected Features', fontsize=18)
plt.savefig('detected_features.png', dpi=100)


# do feature correspondance ... compute distance between each SIFT descriptor
N = r_feat_descr.shape[0]
r = 0.4
crspnd_feat = []
for feat_num, i in enumerate(l_feat_descr):

    # vectorized implementation
    feat = np.tile(i, (N, 1))

    # calculate L2 norm
    dist = np.linalg.norm(feat - r_feat_descr, ord=2, axis=1)

    # best match
    best = np.min(dist)

    # second best match
    s_best = np.sort(dist)[1]

    # find ratio
    feat_ratio = best/s_best

    # if the best feature is a good match, then use it... otherwise continue
    if (feat_ratio < r):
        crspnd_feat.append(((int(round(l_feat_loc[feat_num].pt[0])),
                             int(round(l_feat_loc[feat_num].pt[1]))),
                            (int(round(r_feat_loc[np.argmin(dist)].pt[0])),
                             int(round(r_feat_loc[np.argmin(dist)].pt[1])))))

# concat two images to plot
concat_img = np.hstack((l_img_mkd, r_img_mkd))

# draw lines
offset_x = l_img.shape[1]
offset_y = 0

# plot correspondance
for feat_num, i in enumerate(crspnd_feat):
    left_x = offset_x + i[1][0]
    left_y = offset_y + i[1][1] 
    cv2.line(concat_img, i[0], (left_x, left_y), 0, 1)

plt.figure(figsize=(16, 8))
plt.imshow(cv2.cvtColor(concat_img, cv2.COLOR_BGR2RGB))
plt.axis('off')
plt.title('Corresponding Features (r = {})'.format(r), fontsize=18)
plt.savefig('corresponding_features.png', dpi=100)

# cast to numpy array for easy indexing
crspnd_feat = np.array(crspnd_feat)

# apply RANSAC method to matching pairs
hmgrphy, present = cv2.findHomography(crspnd_feat[:, 1, :].astype(float),
                                      crspnd_feat[:, 0, :].astype(float),
                                      cv2.RANSAC)

# apply homography to the right image
wrpd_r = cv2.warpPerspective(r_img, hmgrphy, (r_img.shape[1]+l_img.shape[1],
                                              r_img.shape[0]))#(l_img.shape[1], l_img.shape[0]))

# extract transformed image
plt.figure(figsize=(8, 4))
plt.imshow(cv2.cvtColor(wrpd_r, cv2.COLOR_BGR2RGB))
plt.title('Warped Right Image', fontsize=18)
plt.axis('off')
plt.savefig('warped_right.png', dpi=100)

# stitch
stitched = np.copy(wrpd_r)
stitched[0:l_img.shape[0], 0:l_img.shape[1]] = l_img

# save stitched
plt.figure(figsize=(16, 8))
plt.imshow(cv2.cvtColor(stitched, cv2.COLOR_BGR2RGB))
plt.title('Stitched Image', fontsize=18)
plt.axis('off')
plt.savefig('stitched.png', dpi=100)
