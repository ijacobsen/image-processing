import numpy as np
import cv2
from matplotlib import pyplot as plt
import feature_detection_lib as fdl

# load image
file_name = 'blocks.jpg'
img = cv2.imread(file_name, 0)

# rotate the image
rot_mat = cv2.getRotationMatrix2D((img.shape[0]/2, img.shape[1]/2), 3, 1)
img_rot = cv2.warpAffine(img, rot_mat, (img.shape[1], img.shape[0]))

# some parameters
sigma = 1
N = 50

# use harris detector to find features
feats = fdl.harris_detector(img, sigma, N)

# get SIFT feature descriptors
feat_dscrpt = fdl.SIFT_descriptor(feats, img, sigma)

# use harris detector to find features
feats_rot = fdl.harris_detector(img_rot, sigma, N)

# get SIFT feature descriptors
feat_dscrpt_rot = fdl.SIFT_descriptor(feats_rot, img, sigma)

crspnd_feat = []
min_dist = []
r = 0.85
# compute distance between each SIFT descriptor
for feat_num, i in enumerate(feat_dscrpt):

    # vectorized implementation
    feat = np.tile(i, (N, 1))

    # calculate L2 norm
    dist = np.linalg.norm(feat - feat_dscrpt_rot, ord=2, axis=1)

    # best match
    best = np.min(dist)

    # second best match
    s_best = np.sort(dist)[1]

    # find ratio
    feat_ratio = best/s_best

    # if the best feature is a good match, then use it... otherwise continue
    if (feat_ratio < r):
        crspnd_feat.append((feats[feat_num], feats_rot[np.argmin(dist)]))

# show detected features
img_feat = np.copy(img)
for i in feats:
    cv2.circle(img_feat, i, 10, 0, 3)

# show detected features
img_feat_rot = np.copy(img_rot)
for i in feats_rot:
    cv2.circle(img_feat_rot, i, 10, 0, 3)

# put pictures together
concat_img = np.hstack((img_feat, img_feat_rot))

# draw lines
offset_x = img.shape[1]
offset_y = 0

for feat_num, i in enumerate(crspnd_feat):
    rot_x = offset_x + i[1][0]
    rot_y = offset_y + i[1][1]
    cv2.line(concat_img, i[0], (rot_x, rot_y), 0, 3)

plt.figure(figsize=(16, 8))
plt.imshow(concat_img, cmap='gray')
plt.savefig('feature_corr.png', dpi=100)
