'''
write a program that can generate SIFT descriptor for each detected feature
point using the program in problem 1. follow the steps:
    a.) generate gradient images Ix and Iy... furthermore, determine the
gradient magnitude and orientation from Ix and Iy at every pixel.
    b.) quantize the orientation of each pixel to one of the N=8 bins. suppose
the original orientation is x. to quantize the entire range of 360 degree to
8 bins, the bin size is q=360/n=45 degree. assuming the orientation is
determined with a range of [0, 360], you can perform quantization using:
        i.) x_q = floor((x+q/2)/q) if x_q=n, x_q=0
        ii.) x_q will range from 0 to 7, with 0 corresponding to degree in
[-22.5, 22.5]
    c.) then for each detected feature point, follow the steps to generate the
SIFT descriptor:
        i.) generate a patch of 16x16 centered at the detected feature point
        ii.) multiply the gradient magnitude with a Gaussian window with
scale=patch width/2
        iii.) generate a HoG for the entire patch using the weighted gradient
magnitude
        iv.) determine the dominant orientation of the patch by detecting the
peak in the HoG determined in (iii).
        v.) generate a HoG for each of the 4x4 cell in the 16x16 patch
        vi.) shift each HoG so that the dominant orientation becomes the first
bin
        vii.) concatenate the HoG for all 16 cells into a single vector
        viii.) normalize the vector (divide each entry by the L2 norm)
        ix.) clip the normalized vector so that entire >0.2 is set to 0.2
        x.) renormalize the vector resulting from (ix)
'''

import numpy as np
import cv2
from matplotlib import pyplot as plt
import feature_detection_lib as fdl

# load image
file_name = 'blocks.jpg'
img = cv2.imread(file_name, 0)

# some parameters
sigma = 1
N = 50

# use harris detector to find features
feats = fdl.harris_detector(img, sigma, N)

# show detected features
feat_img = np.copy(img)
for i in feats:
    cv2.circle(feat_img, i, 7, 0, 1)

# feature vector
feat_vec = np.zeros((N, 128))

# for each detected feature
for feat_num, i in enumerate(feats):

    # generate a patch of size 16x16 centered at the detected feature point
    patch = img[i[1]-8:i[1]+8, i[0]-8:i[0]+8]

    # derive image gradient Ix and Iy
    I_x = fdl.I_x(patch, fdl.G_x(sigma, 4*sigma+1))
    I_y = fdl.I_y(patch, fdl.G_y(sigma, 4*sigma+1))

    # determine the magnitude and orientation for every pixel
    m = np.sqrt(I_x**2 + I_y**2)
    theta = np.arctan((I_y)/(I_x+0.00001))

    # multiply the gradient magnitude with a Gaussian window with sigma = 4
    weighted_mag = m * fdl.G(4, 16)

    # generate weighted orientation histogram
    count, bins = np.histogram(theta, bins=8, weights=weighted_mag)

    # dominant orientation
    d_o = np.argmax(count)

    # empty hog list... will be (16 x 8) when finished
    hog_list = []

    # each 4x4 block
    for j in range(4):
        l_j, h_j = 4*j, 4*(j+1)
        for k in range(4):
            l_k, h_k = 4*k, 4*(k+1)

            # print('indices: {}'.format((l_j, h_j, l_k, h_k)))

            # small (4x4) magnitude
            s_m = m[l_j:h_j, l_k:h_k]

            # small (4x4) orientation
            s_t = theta[l_j:h_j, l_k:h_k]

            # generate weighted histogram of gaussian for small block
            hog, bins = np.histogram(s_t, bins=8, weights=s_m)

            # shift hog into dominant position
            hog = np.roll(hog, -d_o, axis=0)

            # store hog
            hog_list.append(hog)

    # convert to 128 dimensional feature vector
    hog = np.array(hog_list).reshape(1, 128)

    # normalize with L2 norm
    hog = hog / np.linalg.norm(hog, ord=2)

    # nonlinear clipping
    hog[hog > 0.2] = 0.2

    # renormalize
    hog = hog / np.linalg.norm(hog, ord=2)

    # store feature descriptor
    feat_vec[feat_num, :] = hog


'''
plt.imshow(m, cmap='gray')
plt.imshow(theta, cmap='gray')
plt.imshow(np.hstack((I_x, I_y)), cmap='gray')
'''
