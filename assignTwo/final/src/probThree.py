'''
use ISTA functino for image denoising in a block-by-block manner, using
2D DCT basis as the dictionary, and block size of 8x8.
    - read in an image
    - add random noise at a specified level
    - run ISTA on each 8x8 block (without overlapping)
*** try two different noise levels (0.01*255, 0.1*255)
*** try two different lambda values (1, 10)
'''

import numpy as np
import cv2
from matplotlib import pyplot as plt


def soft_thresh(x, T):
    '''
    description:
        - performs soft thresholding.

    inputs:
        - x: vector to threshold
        - T: scalar, thresholding parameter
    outputs:
        - z: vector containing thresholded signal
    '''

    # create copy (default is reference...)
    z = np.copy(x)

    # all values below the threshold will be set to 0
    z[(np.abs(x) < T)] = 0

    # all other values with be nonlinearly mapped
    z[(np.abs(x) - T) > 0] += np.sign(x[(np.abs(x) - T) > 0]) * (-T)

    return z


def ISTA(H, y, lambd):
    '''
    description:
        - runs ISTA algorithm until convergence.

    inputs:
        - H: dictionary array
        - y: signal vector
        - lambd: scalar parameter for thresholding

    outputs:
        - estimate of x
    '''

    # initializations
    o_error = 2
    J = []

    # initial value of x
    x = np.zeros(y.shape)

    # convergence criteria
    converged = False
    epsilon = 1e-6

    # find largest eigenvalue of the covariance matrix of H
    alpha = np.real(np.max(np.linalg.eig(np.matmul(H.T, H))[0]))

    # loop until convergence
    while not converged:

        # compute intermediate value for aesthetics
        x_ = x + np.real(np.matmul(H.T, y - np.matmul(H, x)))

        # perform soft thresholding
        x = soft_thresh(x_, lambd/(2*alpha))

        # check for convergence
        n_error = np.linalg.norm(y - np.matmul(H, x), ord='fro')
        if (np.abs((o_error - n_error)/o_error) < epsilon):
            converged = True
        else:
            o_error = n_error

        # track convergence of cost function
        J.append(n_error + lambd * np.linalg.norm(x, ord=1))

    return x, J


def DCT_basis_gen(N):
    '''
    description:
        - generates 1D discrete cosine transform basis vectors

    input:
        - N, number of basis vectors to generate

    output:
        - array H, consisting of the N DCT basis vectors
    '''

    # angle for H
    k_ = np.tile(np.linspace(0, N-1, N), (N, 1)) * np.pi/(2*N)
    n_ = np.tile(np.linspace(1, 2*(N-1) + 1, N).reshape(N, 1), (1, N))
    angle = n_ * k_

    # magnitude for H
    mag = np.hstack((np.tile(1/np.sqrt(N), (N, 1)),
                     np.tile(np.sqrt(2./N), (N, N-1))))

    # put it together
    H = np.cos(angle) * mag

    return H

# load lena
file_name = 'lena.bmp'
img = cv2.imread(file_name, 0)

# noise level
sigma = 0.1
img_noisey = img + np.random.normal(0, sigma*255, size=img.shape)

# parameter for ISTA
lambd = 25

# filtered image
img_filt = np.zeros(img.shape)

# declare block size
block_sz = 8

# generate basis
N = block_sz
H = DCT_basis_gen(N)

# the dictionary consists of 64 atoms... each atom is a reordering of the
# outer product of the 1D DCT basis vectors... so we flatten and form D
D_ = []
for i in range(0, H.shape[0]):
    for j in range(0, H.shape[1]):
        D_.append(np.outer(H[:, i].T, H[:, j]).reshape(len(H[:, i])**2, 1))
D = np.array(D_).T[0]

# number of steps to take
num_steps_dim0 = (img.shape[0] / block_sz)
num_steps_dim1 = (img.shape[1] / block_sz)

for i in range(num_steps_dim0):
    for j in range(num_steps_dim1):

        # extract and flatten pixels
        y = img_noisey[i*N:(i+1)*N, j*N:(j+1)*N].reshape(N*N, 1)

        # estimate x
        x, J = ISTA(D, y, lambd)

        # reconstruct y
        y_ = np.matmul(D, x).reshape(N, N)

        # filtered image
        img_filt[i*N:(i+1)*N, j*N:(j+1)*N] = y_


# plots
plt.figure(1)
plt.clf()
plt.figure(figsize=(16,16))
plt.subplot(1, 3, 1)
plt.title('Original')
plt.imshow(img, cmap='gray')
plt.axis('off')
plt.subplot(1, 3, 2)
plt.title('Noisey (sigma = {})'.format(sigma))
plt.imshow(img_noisey, cmap='gray')
plt.axis('off')
plt.subplot(1, 3, 3)
plt.title('Filtered (lambda = {})'.format(lambd))
plt.imshow(img_filt, cmap='gray')
plt.axis('off')
plt.savefig('DCT_ISTA_sig={}_lambd={}.png'.format(sigma, lambd))


norm_n_f = np.linalg.norm(img_noisey - img_filt)
norm_f = np.linalg.norm(img - img_filt)
norm_n = np.linalg.norm(img_noisey - img)
print('n_f: {}, f: {}, n: {}'.format(norm_n_f, norm_f, norm_n))
