'''
test the ISTA function for a dimension N=4 vector with sparse representation
using 1D DCT.
    - generate a dictionary H that includes all of the 1D DCT basis vectors
of dimension N
    - generate a coefficient vector x that is relatively sparse
    - generate data vector y=Hx
    *** use y directly, then perturb it to simulate noise
    - run the program to see whether it provides correct solution x, or how
    far it is from the real solution
    
write a function DCT_basis_gen(N) which takes dimension N as input and returns
1D DCT basis vectors.
'''

import numpy as np


def soft_thresh(x, T):
    '''
    description:
        - performs soft thresholding.

    inputs:
        - x: vector to threshold
        - T: scalar, thresholding parameter
    outputs:
        - z: vector containing thresholded signal
    '''

    # create copy (default is reference...)
    z = np.copy(x)

    # all values below the threshold will be set to 0
    z[(np.abs(x) < T)] = 0

    # all other values with be nonlinearly mapped
    z[(np.abs(x) - T) > 0] += np.sign(x[(np.abs(x) - T) > 0]) * (-T)

    return z


def ISTA(H, y, lambd):
    '''
    description:
        - runs ISTA algorithm until convergence.

    inputs:
        - H: dictionary array
        - y: signal vector
        - lambd: scalar parameter for thresholding

    outputs:
        - estimate of x
    '''
    
    # initializations
    o_error = .5
    J = []

    # initial value of x
    x = np.zeros(y.shape)

    # convergence criteria
    converged = False
    epsilon = 1e-6

    # find largest eigenvalue of the covariance matrix of H
    alpha = np.max(np.linalg.eig(np.matmul(H.T, H))[0])

    # loop until convergence
    while not converged:

        # compute intermediate value for aesthetics... dont need alpha
        x_ = x + np.matmul(H.T, y - np.matmul(H, x))

        # perform soft thresholding
        x = soft_thresh(x_, lambd/(2*alpha))

        # check for convergence
        n_error = np.linalg.norm(y - np.matmul(H, x), ord='fro')
        if (np.abs((o_error - n_error)/o_error) < epsilon):
            converged = True
        else:
            o_error = n_error

        # track convergence of cost function
        J.append(n_error + lambd * np.linalg.norm(x, ord=1))

    return x, J


def DCT_basis_gen(N):
    '''
    description:
        - generates 1D discrete cosine transform basis vectors

    input:
        - N, number of basis vectors to generate

    output:
        - array H, consisting of the N DCT basis vectors
    '''

    # angle for H
    k_ = np.tile(np.linspace(0, N-1, N), (N, 1)) * np.pi/(2*N)
    n_ = np.tile(np.linspace(1, 2*(N-1) + 1, N).reshape(N, 1), (1, N))
    angle = n_ * k_

    # magnitude for H
    mag = np.hstack((np.tile(1/np.sqrt(N), (N, 1)),
                     np.tile(np.sqrt(2./N), (N, N-1))))

    # put it together
    H = np.cos(angle) * mag

    return H

# test using Discrete Cosine Transform

# generate basis
N = 4
H = DCT_basis_gen(N)

# some random sparse vector
x = np.array([[0, 100, 0, 0]]).T

# actual value of y
y = np.matmul(H, x)

# add some noise to y
y_noise = y + np.random.randn(y.shape[0], y.shape[1])

# ISTA estimate of y
lambd = 0.01
x_, J = ISTA(H, y, lambd)
x_noise, J_noise = ISTA(H, y_noise, lambd)

# results
if (np.linalg.norm(x - x_, ord='fro') < 1e-2):
    print('successfully converged to x without noise')

print('error without noise = {}'.format(np.linalg.norm(x - x_, ord='fro')))

if (np.linalg.norm(x - x_noise, ord='fro') < 1e-2):
    print('successfully converged to x with noise')

print('error with noise = {}'.format(np.linalg.norm(x - x_noise, ord='fro')))
