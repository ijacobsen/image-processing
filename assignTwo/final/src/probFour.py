'''

perform wavelet based denoising.

- modify ISTA function to work directly with 2D images.
- note: Hx is inverse 2D transform, and H'y is forward 2D transform
- y is the noisy input image, and x are the wavelet transform coefficients
represented in the data structure that is returned by th2D wavelet transform.

- program Hx as a function inverse_transform(x)
- program H'y as a function forward_transform(y)
- the update of x can be done for each individual element in x.

- use pywt.wavedec() for forward transform
- use pywt.waverec() for inverse transform

- pick one noise level and lambda, and show results using two different wavelets:
    - Haar wavelet
    - Daubechies 8/8 wavelet
- use 3 level decomposition.

- include plots of:
    - original image
    - noisy image
    - wavelet transform of the noisy image
    - wavelet transform of the final denoised image
    - the final denoised image

'''
import numpy as np
import pywt
import cv2
from matplotlib import pyplot as plt


def soft_thresh(x, T):
    '''
    description:
        - performs soft thresholding.

    inputs:
        - x: vector to threshold
        - T: scalar, thresholding parameter
    outputs:
        - z: vector containing thresholded signal
    '''

    # create copy (default is reference...)
    z = np.copy(x)

    # all values below the threshold will be set to 0
    z[(np.abs(x) < T)] = 0

    # all other values with be nonlinearly mapped
    z[(np.abs(x) - T) > 0] += np.sign(x[(np.abs(x) - T) > 0]) * (-T)

    return z


def wavelet_denoise(y, lambd, wavelet_type, levels, og_img):
    '''
    description:
        - denoises an image using soft thresholding in the wavelet domain.

    inputs:
        - y: noisy image array
        - lambd: scalar parameter for thresholding
        - wavelet_type: type of wavelet transformation to use

    outputs:
        - img_filtered: denoised image
        - wv_coeffs: wavelet coefficients
    '''
    
    # initializations
    error = []
    o_error = 2
    
    # convergence criteria
    epsilon = 1e-1
    converged = False

    while not converged:

        # perform forward wavelet transform (H'y)
        coeffs = pywt.wavedec2(y, wavelet_type, levels)

        # adhere to data structure that pywt uses
        coeffs_denoised = []
        coeffs_denoised.append(soft_thresh(coeffs[0], lambd))

        # soft threshold each set of coefficients at each level
        for lvls in coeffs[1:]:
            temp_list = []
            for cfs in lvls:
                temp_list.append(soft_thresh(cfs, lambd))
            coeffs_denoised.append(temp_list)

        # reconstruct image using the filtered coefficients
        img_filtered = pywt.waverec2(coeffs_denoised, wavelet_type)

        # check for convergence
        n_error = np.linalg.norm(og_img - img_filtered, ord='fro')

        if (np.abs((o_error - n_error)/o_error) < epsilon):
            converged = True
        else:
            o_error = n_error

        # update image to filter
        y = img_filtered

        # track error
        error.append(o_error)

    return img_filtered, coeffs_denoised, error


# load lena
file_name = 'lena.bmp'
img = cv2.imread(file_name, 0)

# noise level
sigma = .1
img_noisey = img + np.random.normal(0, sigma*255, size=img.shape)

# parameter for thresholding (from Selesnick notes)
lambd = 7

# wavelet arguments
wavelet_type = 'db8'
levels = 3

# filtered image
img_filt, coeffs, error = wavelet_denoise(img_noisey, lambd, wavelet_type, levels, img)


# comparison between original, noisy, and denoised
plt.figure(figsize=(24, 8))
plt.suptitle('{} transform, {} levels'.format(wavelet_type, levels),
             fontsize=24)
plt.subplot(1, 3, 1)
plt.imshow(img, cmap='gray')
plt.axis('off')
plt.title('Original Image')
plt.subplot(1, 3, 2)
plt.imshow(img_noisey, cmap='gray')
plt.axis('off')
plt.title('Noisey Image (sigma = {})'.format(sigma))
plt.subplot(1, 3, 3)
plt.imshow(img_filt, cmap='gray')
plt.axis('off')
plt.title('Wavelet Denoised Image (lambda = {})'.format(lambd))
plt.savefig('p4_images_{}_levels={}.png'.format(wavelet_type, levels))

# pyramid plot of denoised image
reconstructed = pywt.waverec2(coeffs[:-1], wavelet_type)
plt.figure(figsize=(16, 16))
plt.suptitle('level {} of the {} transform of denoised image'.format(levels, wavelet_type),
             fontsize=24)
plt.subplot(2, 2, 1)
plt.imshow(reconstructed, cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 2)
plt.imshow(coeffs[-1][0], cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 3)
plt.imshow(coeffs[-1][1], cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 4)
plt.imshow(coeffs[-1][2], cmap='gray')
plt.axis('off')
plt.savefig('p4_wavelets_denoised_{}_levels={}.png'.format(wavelet_type, levels))

# error plot
plt.figure(figsize = (8, 8))
plt.title('Error (Frob. Norm)', fontsize=24)
plt.plot(np.linspace(1, len(error), len(error)), error)
plt.xlabel('Iterations')
plt.ylabel('Error')
plt.savefig('p4_error_{}_levels={}.png'.format(wavelet_type, levels))

# pyramid plot of noisy image
coeff_n = pywt.wavedec2(img_noisey, wavelet_type, levels)
reconstructed_noisy = pywt.waverec2(coeff_n[:-1], wavelet_type)
plt.figure(figsize=(16, 16))
plt.suptitle('level {} of the {} transform of noisy image'.format(levels, wavelet_type), fontsize=24)
plt.subplot(2, 2, 1)
plt.imshow(reconstructed_noisy, cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 2)
plt.imshow(coeff_n[-1][0], cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 3)
plt.imshow(coeff_n[-1][1], cmap='gray')
plt.axis('off')
plt.subplot(2, 2, 4)
plt.imshow(coeff_n[-1][2], cmap='gray')
plt.axis('off')
plt.savefig('p4_wavelets_noisy_{}_levels={}.png'.format(wavelet_type, levels))
