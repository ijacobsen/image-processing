'''
implement ISTA algorithm for solving the LASSO problem

- takes in the dictionary matrix H, signal vector y, and parameter lambda
- returns the solution x

- determine parameter alpha = maxeig(H'H)
- start with any initial condition (i.e. x_0 = 0)
- convergence condition as (error_ratio-new_error)/old_error < T (T=1e-1),
where the error refers to the residual L2 norm ||y-Hx||

ISTA (Iterative Soft Thresholding Algorithm)
- solves LASSO problem through majorization-minimization
- each step solves a single variable problem using soft thresholding
- 
'''

import numpy as np


def soft_thresh(x, T):
    '''
    description:
        - performs soft thresholding.

    inputs:
        - x: vector to threshold
        - T: scalar, thresholding parameter
    outputs:
        - z: vector containing thresholded signal
    '''

    # create copy (default is reference...)
    z = np.copy(x)

    # all values below the threshold will be set to 0
    z[(np.abs(x) < T)] = 0

    # all other values with be nonlinearly mapped
    z[(np.abs(x) - T) > 0] += np.sign(x[(np.abs(x) - T) > 0]) * (-T)

    return z


def ISTA(H, y, lambd):
    '''
    description:
        - runs ISTA algorithm until convergence.

    inputs:
        - H: dictionary array
        - y: signal vector
        - lambd: scalar parameter for thresholding

    outputs:
        - estimate of x
    '''
    
    # initializations
    o_error = 1
    J = []

    # initial value of x
    x = np.zeros(y.shape)

    # convergence criteria
    converged = False
    epsilon = 1e-4

    # find largest eigenvalue of the covariance matrix of H
    alpha = np.max(np.linalg.eig(np.matmul(H.T, H))[0])

    # loop until convergence
    while not converged:

        # compute intermediate value for aesthetics
        x_ = x + (1/alpha) * np.matmul(H.T, y - np.matmul(H, x))

        # perform soft thresholding
        x = soft_thresh(x_, lambd/(2*alpha))

        # check for convergence
        n_error = np.linalg.norm(y - np.matmul(H, x), ord='fro')
        if (np.abs((o_error - n_error)/o_error) < epsilon):
            converged = True
        else:
            o_error = n_error

        # track convergence of cost function
        J.append(n_error + lambd * np.linalg.norm(x, ord=1))

    return x, J

# test using Hadamard Transform
H = np.array([[.5, .5, .5, .5], [.5, .5, -.5, -.5],
              [.5, -.5, -.5, .5], [.5, -.5, .5, -.5]]).T

f = np.array([[1, 2, 3, 4]]).T
t_ = np.array([[5, -2, 0, -1]]).T
lambd = .01

# check ISTA
t, J = ISTA(H, f, lambd)

# results
if (np.linalg.norm(t - t_, ord='fro') < 1e-2):
    print('successfully converged to solution')
